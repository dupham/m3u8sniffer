Forked from: https://github.com/Leenshady/m3u8Sniffer
A Chrome plug-in that scans the m3u8 link in a web page.  

![image](https://user-images.githubusercontent.com/27936579/109785249-e830cf80-7c46-11eb-8a89-e015ee18ae20.png)

![image](https://user-images.githubusercontent.com/27936579/109785314-f848af00-7c46-11eb-91be-a59cb4df8b4b.png)

